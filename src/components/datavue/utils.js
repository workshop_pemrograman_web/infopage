export const getArrayIndexes = (length, limit, offset) => {
    let pages = Math.ceil(length / limit);
      
    let from = (offset - 1) * limit;
      
    let to =
        (offset === pages
          ? length
          : from + limit) - 1;
    
    return { from, to }
}